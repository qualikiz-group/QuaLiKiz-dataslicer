# QuaLiKiz-dataslicer
This repository contains the super hacky tool to scroll through
[QuaLiKiz](https://qualikiz.com) datasets generated for the QuaLiKiz
Neural Network (QLKNN) family of projects, of which the reference
dataset (in netCDF4 and HDF5 format) can be found on
[Zenodo](https://zenodo.org/record/3497066). This repo is always in flux
and personally hacked to work with specific [bokeh](https://bokeh.org/)
versions, and makes heavy use of the hobby-project [Bokeh integration]
(https://gitlab.com/qualikiz-group/bokeh-ion-rangeslider),
which wraps the excellent
[ion.rangeSlider](https://github.com/IonDen/ion.rangeSlider/) JavaScript
library


# Docker Environment
We try as much as possible to copy the bokeh Docker, as described in
[BEP appendix Bokeh development docker container](https://github.com/bokeh/bokeh/wiki/Docker-dev-container#bokeh-development-docker-container)

These containers are live-builds by GitHub on request, or on most commits, and
can be found in [Bokeh's GitHub actions](https://github.com/bokeh/bokeh/actions/workflows/bokeh-docker-build.yml)
